import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import nl.rabobank.bankingapp.transactions.Config;
import nl.rabobank.bankingapp.transactions.TransactionService;


public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext appContext = new AnnotationConfigApplicationContext(Config.class);
        TransactionService transactionService = appContext.getBean(TransactionService.class);
        System.out.println(transactionService.getTotalAmount());

    }
}