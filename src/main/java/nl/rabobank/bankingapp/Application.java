package nl.rabobank.bankingapp;

import org.h2.tools.Server;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;

import java.io.IOException;
import java.sql.SQLException;

@SpringBootApplication
public class Application {

    DatabasePopulation dbPopulation;

    public Application(DatabasePopulation dbPopulation) {
        this.dbPopulation = dbPopulation;
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean(initMethod = "start", destroyMethod = "stop")
    public Server h2server() throws SQLException{
        return Server.createTcpServer("-tcp", "-tcpAllowOthers", "-tcpPort", "9092");
    }

    @EventListener(ApplicationReadyEvent.class)
    public void doAfterApplicationReady() throws IOException {
       //dbPopulation.populateDB();
    }

}
