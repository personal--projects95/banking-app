package nl.rabobank.bankingapp.transactions;

import lombok.RequiredArgsConstructor;
import nl.rabobank.bankingapp.transactions.repositories.TransactionsRepository;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.security.Principal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
public class TransactionController {

    private final TransactionService transactionService;
    private final TransactionsRepository transRepo;

    @GetMapping( "/transactions")
    public List<Transaction> transactionsList(@RequestParam String IBAN, Principal principal) {
        //System.out.println(principal.getName());
        List<Transaction> transactions = transactionService.getTransactionLists(IBAN, principal.getName());
        return transactions;
    }

    /*@GetMapping( "/transactions/selection")
    public List<Transaction> transactionsListBetweenDates(@RequestParam String IBAN) {
        List<Transaction> transactions = transactionService.getTransBetweenDates(IBAN, LocalDate.of(2023, 03, 01), LocalDate.of(2023, 07,01));
        return transactions;
    }*/

    @GetMapping( "/transactions/balance")
    public BigDecimal getBalance(@RequestParam String IBAN, Principal principal) {
        return transactionService.getBalance(IBAN, principal.getName());
    }

    @GetMapping( "/transactions/spendings/CO2/dates")
    public Map<String, Map<String, BigDecimal>> transactionsSpendingsBetweenDatesInCO2(@RequestParam String IBAN, @RequestParam @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate startDate, @RequestParam @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate endDate, Principal principal) {
        //List<Transaction> transactions = transactionService.getTransactionLists(IBAN);
        return transactionService.calculateSpendingsBetweenDatesInCO2(startDate, endDate, transactionService.getTransactionLists(IBAN, principal.getName()));
    }

    @GetMapping( "/transactions/spendings/euro/dates")
    public Map<String, Map<String, BigDecimal>> transactionsSpendingsBetweenDatesInEuro(@RequestParam String IBAN, @RequestParam @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate startDate, @RequestParam @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate  endDate, Principal principal) {
        //List<Transaction> transactions = transactionService.getTransactionLists(IBAN);
        return transactionService.calculateSpendingsBetweenDatesInEuro(startDate, endDate, transactionService.getTransactionLists(IBAN, principal.getName()));
    }////////////////////

    @GetMapping( "/transactions/spendings/CO2")
    public Map<String, Map<String, BigDecimal>> transactionsSpendingsInCO2(@RequestParam String IBAN, Principal principal) {
        //List<Transaction> transactions = transactionService.getTransactionLists(IBAN);
        return transactionService.calculateSpendingsInCO2(transactionService.getTransactionLists(IBAN, principal.getName()));
    }

    @GetMapping( "/transactions/spendings/euro")
    public Map<String, Map<String, BigDecimal>> transactionsSpendingsInEuro(@RequestParam String IBAN, Principal principal) {
        //List<Transaction> transactions = transactionService.getTransactionLists(IBAN);
        return transactionService.calculateSpendingsInEuro(transactionService.getTransactionLists(IBAN, principal.getName()));
    }

    @PostMapping(value="/transactions")
   // public Transaction postTrans(@RequestBody Transaction transaction){
    //public void postTrans(@RequestParam String receiverNameAccount, @RequestParam String receiverIBAN, @RequestParam String amount, @RequestParam String description, @RequestParam String username, @RequestParam String senderIBAN, Principal principal){
    public void postTrans(@RequestParam String receiverNameAccount, @RequestParam String receiverIBAN, @RequestParam String amount, @RequestParam String description, @RequestParam String senderIBAN, Principal principal){

        //transactionService.addTransaction(transaction);
       // return transaction;
        //System.out.println("controller");
        transactionService.addTransaction(receiverNameAccount, receiverIBAN, amount, description, senderIBAN, principal.getName());

        //transactionService.addTransaction(receiverNameAccount, receiverIBAN, amount, description, username, senderIBAN);
    }

    @PostMapping(value="/transactions/test")
    // public Transaction postTrans(@RequestBody Transaction transaction){
    //public void postTrans(@RequestParam String receiverNameAccount, @RequestParam String receiverIBAN, @RequestParam String amount, @RequestParam String description, @RequestParam String username, @RequestParam String senderIBAN, Principal principal){
    public void postTransTest(@RequestBody Transaction transaction, Principal principal){

        //transactionService.addTransaction(transaction);
        // return transaction;
        //System.out.println("controller");
        transactionService.addTransactionTest(transaction, principal.getName());

        //transactionService.addTransaction(receiverNameAccount, receiverIBAN, amount, description, username, senderIBAN);
    }


    @PutMapping(value="/transactions/update")
    public void updateTrans(@RequestParam String receiverIBAN, @RequestParam String amount, @RequestParam String senderIBAN, @RequestParam String description, @RequestParam String category, Principal principal){
        transactionService.updateTransaction(receiverIBAN, new BigDecimal(amount), senderIBAN, description, category, principal.getName());
    }

    //@GetMapping(value="/transactions/update/test")
    //public Transaction updateTransTest(@RequestParam String receiverIBAN, @RequestParam BigDecimal amount, @RequestParam String senderIBAN, @RequestParam String category){
       // return transRepo.getTransactionByAmountAndSenderIBANAndReceiverIBAN(amount, senderIBAN, receiverIBAN);
    //}

    @GetMapping( "/transactions/insights")
    public List<Transaction> transactionsListForInsights(@RequestParam String IBAN, @RequestParam String category, @RequestParam String month, @RequestParam String startDate, @RequestParam String endDate) {
        List<Transaction> transactions = transactionService.getTransactionListsForInsights(IBAN, category, month, startDate, endDate);
        return transactions;
    }
}


