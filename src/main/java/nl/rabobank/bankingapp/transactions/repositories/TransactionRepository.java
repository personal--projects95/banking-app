package nl.rabobank.bankingapp.transactions.repositories;

import nl.rabobank.bankingapp.transactions.Transaction;

import java.util.List;

public interface TransactionRepository {
    List<Transaction> getAllTransactions();
}
