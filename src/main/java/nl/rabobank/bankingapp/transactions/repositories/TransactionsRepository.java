package nl.rabobank.bankingapp.transactions.repositories;

import lombok.RequiredArgsConstructor;
import nl.rabobank.bankingapp.transactions.Transaction;
import nl.rabobank.bankingapp.users.repositories.UserRepository;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Repository
public interface TransactionsRepository extends JpaRepository<Transaction, Long> {


    List<Transaction> getTransactionByReceiverIBANOrSenderIBAN(String receiverIBAN, String senderIBAN, Sort sort);
    List<Transaction> getTransactionByReceiverIBANOrSenderIBANAndDateIsBetween(String receiverIBAN, String senderIBAN, LocalDate startDate, LocalDate endDate);

    Transaction getTransactionByAmountAndSenderIBANAndReceiverIBANAndDescription(BigDecimal amount, String SenderIBAN, String ReceiverIBAN, String description);

    //List<Transaction> getTransactionByReceiverIBANOrSenderIBANAndCategory(String receiverIBAN, String senderIBAN, String category);
   // Transaction getTransactionByDateBetween



}
