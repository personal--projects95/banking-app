package nl.rabobank.bankingapp.transactions;

import nl.rabobank.bankingapp.accounts.repositories.AccountRepository;
import nl.rabobank.bankingapp.transactions.repositories.TransactionRepository;
import nl.rabobank.bankingapp.transactions.repositories.TransactionsRepository;
import nl.rabobank.bankingapp.users.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

    @Autowired
    TransactionsRepository transactionsRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AccountRepository accountRepository;

    /*  @Bean
    TransactionRepository transactionRepository() {
        return new TransactionsInput();
    }*/

    @Bean
    TransactionService transactionService() {
        return new TransactionService(transactionsRepository, userRepository, accountRepository);
    }
}
