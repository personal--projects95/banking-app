package nl.rabobank.bankingapp.transactions;

import java.util.HashMap;

public class TransactionsStandardCO2 {
    static HashMap<String, Integer> standardCO2 = new HashMap<>();

    static {
        standardCO2.put("groceries", 802);
        standardCO2.put("groundTravel", 83);
        standardCO2.put("utilities", 83);
        standardCO2.put("housing", 83);
        standardCO2.put("airTravel", 1572);
        standardCO2.put("restaurant", 802);
        standardCO2.put("others", 83);

    }
}
