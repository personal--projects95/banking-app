package nl.rabobank.bankingapp.transactions;

import java.util.HashMap;

public class TransactionsKeyWords {
    static HashMap<String, String> keyWords = new HashMap<>();

    static {
        keyWords.put("albert", "groceries");
        keyWords.put("coop", "groceries");
        keyWords.put("heijn", "groceries");
        keyWords.put("lidl", "groceries");
        keyWords.put("plus", "groceries");
        keyWords.put("aldi", "groceries");
        keyWords.put("spar", "groceries");
        keyWords.put("jumbo", "groceries");
        keyWords.put("rent", "housing");
        keyWords.put("mortgage", "housing");
        keyWords.put("eneco", "utilities");
        keyWords.put("vitens", "utilities");
        keyWords.put("groundtravel", "groundTravel");
        keyWords.put("arriva", "groundTravel");
        keyWords.put("wizzair", "airTravel");
        keyWords.put("klm", "airTravel");
        keyWords.put("airfrance", "airTravel");
        keyWords.put("tarom", "airTravel");
        keyWords.put("ryanair", "airTravel");
        keyWords.put("tui", "airTravel");
        keyWords.put("transavia", "airTravel");
        keyWords.put("restaurant", "restaurant");
    }

}
