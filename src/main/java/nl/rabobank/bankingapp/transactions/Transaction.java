package nl.rabobank.bankingapp.transactions;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import nl.rabobank.bankingapp.accounts.Account;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static nl.rabobank.bankingapp.transactions.TransactionsStandardCO2.standardCO2;

@Entity
@Table(name = "transactions")
public class Transaction {
    @Id
    @GeneratedValue
    private Long id;
    //@ManyToOne
   // @JoinColumn(name = "sender_bank_account_id")
   // public Account senderAccount;

    @Column(nullable = false, length = 50)
    public String senderIBAN;

    @Column(nullable = false, length = 200)
    private String senderNameAccount;
    @Column(nullable = false, length = 50)

    public String receiverIBAN;

   // @ManyToOne
   // @JoinColumn(name = "receiver_bank_account_id")
   // public Account receiverAccount;

    @Column(nullable = false, length = 200)
    private String receiverNameAccount;
    @Column(nullable = false, length = 30)
    private BigDecimal amount;

    @Column(nullable = false, length = 30)
    private BigDecimal CO2;

    @Column(nullable = false, length = 100)
    private String description;

    @Column(nullable = false, length = 100)
    private String category;


    @Column(nullable = false, length = 30)
    private LocalDate date;


    public Transaction(String receiverIBAN, String receiverNameAccount, BigDecimal amount, String description, LocalDate date) {
        this.receiverIBAN = receiverIBAN;
        this.receiverNameAccount = receiverNameAccount;
        this.amount = amount;
        this.description = description;
        this.date = date;
    }

    public Transaction(String senderIBAN, String receiverIBAN, String receiverNameAccount, BigDecimal amount, String description, LocalDate date, String category) {
        this.senderIBAN = senderIBAN;
        this.receiverIBAN = receiverIBAN;
        this.receiverNameAccount = receiverNameAccount;
        this.amount = amount;
        this.description = description;
        this.date = date;
        this.category = category;
    }

    public Transaction(String senderIBAN, String senderNameAccount,  String receiverIBAN, String receiverNameAccount, BigDecimal amount, String description, LocalDate date) {
        this.senderIBAN = senderIBAN;
        this.senderNameAccount = senderNameAccount;
        this.receiverIBAN = receiverIBAN;
        this.receiverNameAccount = receiverNameAccount;
        this.amount = amount;
        this.description = description;
        this.date = date;
    }

    public Transaction() {
    }

    public String getCategory() {
        return category;
    }



    public void setCategory(String category) {
        this.category = category;
    }


    public String getReceiverNameAccount() {
        return receiverNameAccount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public BigDecimal getCO2() {
        return CO2;
    }

    public void setCO2(BigDecimal CO2) {
        this.CO2 = CO2;
    }

    public String getDescription() {
        return description;
    }

    public String getSenderIBAN() {
        return senderIBAN;
    }

    public String getReceiverIBAN() {
        return receiverIBAN;
    }

    public Long getId() {
        return id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getSenderNameAccount() {
        return senderNameAccount;
    }

    public void setSenderNameAccount(String senderNameAccount) {
        this.senderNameAccount = senderNameAccount;
    }

    @JsonIgnore
    public String getCategorisation(){
        String categorisation = "others";
        for (String key : TransactionsKeyWords.keyWords.keySet()){
           if (getReceiverNameAccount().toLowerCase().contains(key) || (getDescription().toLowerCase().contains(key))){
                categorisation = TransactionsKeyWords.keyWords.get(key);
                    }
                }
        return categorisation;
    }

    public BigDecimal calculateCO2EmissionInGrams(){
        BigDecimal CO2Emissions = new BigDecimal(0);
        String category = getCategorisation();
        CO2Emissions = new BigDecimal(standardCO2.get(category)).multiply(getAmount());

        return CO2Emissions;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", senderIBAN='" + senderIBAN + '\'' +
                ", senderNameAccount='" + senderNameAccount + '\'' +
                ", receiverIBAN='" + receiverIBAN + '\'' +
                ", receiverNameAccount='" + receiverNameAccount + '\'' +
                ", amount=" + amount +
                ", CO2=" + CO2 +
                ", description='" + description + '\'' +
                ", category='" + category + '\'' +
                ", date=" + date +
                '}';
    }
}
