package nl.rabobank.bankingapp.transactions;

import nl.rabobank.bankingapp.accounts.repositories.AccountRepository;
import nl.rabobank.bankingapp.transactions.repositories.TransactionsRepository;
import nl.rabobank.bankingapp.users.repositories.UserRepository;
import org.springframework.data.domain.Sort;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TransactionService {

    //final TransactionsInput inputTransactions = new TransactionsInput();
    // final ArrayList<Transaction> transactionsList = inputTransactions.populateTransactions();

    private TransactionsRepository transactionsRepository;
    private UserRepository userRepository;

    private AccountRepository accountRepository;


    public TransactionService(TransactionsRepository transactionsRepository, UserRepository userRepository, AccountRepository accountRepository) {
        this.transactionsRepository = transactionsRepository;
        this.userRepository = userRepository;
        this.accountRepository = accountRepository;
    }

  /* public List<Transaction> getTransactionLists(String senderIBAN){
        List<Transaction> transactions = new ArrayList<>();
        for (Transaction transaction :
                transactionsRepository.findAll() ) {
            if (transaction.getSenderIBAN().equals(senderIBAN) || transaction.getReceiverIBAN().equals(senderIBAN)){
                transactions.add(new Transaction(transaction.getSenderIBAN() ,transaction.getReceiverIBAN(), transaction.getReceiverNameAccount(), transaction.getAmount(), transaction.getDescription(), transaction.getDate(), transaction.getCategory()));
            }
        }
        return transactions;
    }*/

    public boolean userHasThisIBAN(String username, String IBAN) {
        List<String> accountsList = accountRepository.getIbansByUsername(username);
        if (accountsList.contains(IBAN)) {
            return true;
        }
        return false;
    }

    public List<Transaction> getTransactionLists(String IBAN, String username) {
        List<Transaction> transactions = new ArrayList<>();
        if (userHasThisIBAN(username, IBAN)) {
            Sort sort = Sort.by(Sort.Order.desc("date"));
            transactions = transactionsRepository.getTransactionByReceiverIBANOrSenderIBAN(IBAN, IBAN, sort);
            //transactions.sort(Comparator.comparing(Transaction::getDate));
            return transactions;
        }
        return transactions;
    }

    public List<Transaction> getTransactionListsForInsights(String IBAN, String category, String month, String startDate, String endDate) {
        List<Transaction> transactions = getTransactionsForIBAN(IBAN);

        if (startDate.equals("null")) {
            return getInsightsOverall(category, month, transactions);
        }
        return getInsightsForDates(category, startDate, endDate, transactions);

    }

    private List<Transaction> getTransactionsForIBAN(String IBAN) {
        return transactionsRepository
                .getTransactionByReceiverIBANOrSenderIBAN(IBAN, IBAN, Sort.by(Sort.Order.asc("date")));
    }

    private static List<Transaction> getInsightsForDates(String category, String startDate, String endDate, List<Transaction> transactions) {
        List<Transaction> transactionsInsightsForDates;
        LocalDate formattedStartDate = parseDate(startDate);
        LocalDate formattedEndDate = parseDate(endDate);
        transactionsInsightsForDates = transactions.stream()
                .filter(inTheDateRange(formattedStartDate, formattedEndDate))
                .collect(Collectors.toList());
        if (!category.contains("Total")) {
            transactionsInsightsForDates = transactions.stream()
                    .filter(inTheDateRange(formattedStartDate, formattedEndDate))
                    .filter (transaction -> transaction.getCategory().equals(category))
                    .collect(Collectors.toList());
        }
        return transactionsInsightsForDates;
    }

    private static LocalDate parseDate(String Date) {
        return LocalDate.parse(Date, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }

    private static Predicate<Transaction> inTheDateRange(LocalDate startDate, LocalDate endDate) {
        return transaction -> transaction.getDate().isAfter(startDate.minusDays(1)) &&
                transaction.getDate().isBefore(endDate.plusDays(1));
    }

    private static List<Transaction> getInsightsOverall(String category, String month, List<Transaction> transactions) {
        List<Transaction> transactionsInsightsForMonth;
        String[] parts = month.split("-");
        int targetYear = Integer.parseInt(parts[0]);
        int targetMonthNumber = Month.valueOf(parts[1].toUpperCase()).getValue();
        transactionsInsightsForMonth = transactions.stream()
                .filter(selectBasedOnYearAndMonth(targetYear, targetMonthNumber))
                .collect(Collectors.toList());
        if (!category.contains("Total")) {
            transactionsInsightsForMonth = transactions.stream()
                    .filter(selectBasedOnYearAndMonth(targetYear, targetMonthNumber))
                    .filter(transaction -> transaction.getCategory().equals(category))
                    .collect(Collectors.toList());
        }
        return transactionsInsightsForMonth;
    }

    private static Predicate<Transaction> selectBasedOnYearAndMonth(int targetYear, int targetMonthNumber) {
        return transaction -> transaction.getDate().getYear() == targetYear &&
                (Month.valueOf(transaction.getDate().getMonth().toString().toUpperCase()).getValue()) == targetMonthNumber;
    }

    public BigDecimal getTotalAmount() {
        BigDecimal totalAmount = new BigDecimal(0);
        for (Transaction transaction : transactionsRepository.findAll()) {
            totalAmount = totalAmount.add(transaction.getAmount());
        }
        return totalAmount;
    }

    public Map<String, BigDecimal> getTotalAmount(List<Transaction> listOfTransactions) {
        BigDecimal totalAmount = new BigDecimal(0);
        Map<String, BigDecimal> totalSpendings = new HashMap<>();
        for (Transaction transaction : listOfTransactions) {
            totalAmount = totalAmount.add(transaction.getAmount());
        }
        return totalSpendings;
    }


    public static Map<String, BigDecimal> calculateSummarySpendingsBetweenDates(LocalDate startDate, LocalDate endDate, List<Transaction> listOfTransactions) {
        Map<String, BigDecimal> summarySpendings = new HashMap<>();
        for (Transaction transaction : listOfTransactions) {
            if (isBetweenDates(startDate, endDate, transaction)) {
                String category = transaction.getCategorisation();
                BiFunction<BigDecimal, BigDecimal, BigDecimal> computedAmounts = BigDecimal::add;
                summarySpendings.merge(category, transaction.getAmount(), (u, v) -> computedAmounts.apply(summarySpendings.get(category), transaction.getAmount()));
            }
        }
        summarySpendings.put("CO2", calculateCO2EmissionsForTransList(listOfTransactions));
        return summarySpendings;
    }

    public static Map<String, BigDecimal> calculateSummarySpendings(List<Transaction> listOfTransactions) {
        Map<String, BigDecimal> summarySpendings = new HashMap<>();
        for (Transaction transaction : listOfTransactions) {
            String category = transaction.getCategorisation();
            BiFunction<BigDecimal, BigDecimal, BigDecimal> computedAmounts = BigDecimal::add;
            summarySpendings.merge(category, transaction.getAmount(), (u, v) -> computedAmounts.apply(summarySpendings.get(category), transaction.getAmount()));
        }
        summarySpendings.put("CO2", calculateCO2EmissionsForTransList(listOfTransactions));
        return summarySpendings;
    }

    public static Map<String, BigDecimal> calculateSummarySpendingsInCO2(List<Transaction> listOfTransactions) {
        Map<String, BigDecimal> summarySpendings = new HashMap<>();
        for (Transaction transaction : listOfTransactions) {
            String category = transaction.getCategory();
            BiFunction<BigDecimal, BigDecimal, BigDecimal> computedAmounts = BigDecimal::add;
            summarySpendings.merge(category, transaction.calculateCO2EmissionInGrams(), (u, v) -> computedAmounts.apply(summarySpendings.get(category), transaction.calculateCO2EmissionInGrams()));
        }
        summarySpendings.put("Total[g]", calculateCO2EmissionsForTransList(listOfTransactions));
        return summarySpendings;
    }

    public static Map<String, BigDecimal> calculateSummarySpendingsInEuro(List<Transaction> listOfTransactions) {
        Map<String, BigDecimal> summarySpendings = new HashMap<>();
        BigDecimal totalAmount = new BigDecimal(0);
        for (Transaction transaction : listOfTransactions) {
            String category = transaction.getCategory();
            BiFunction<BigDecimal, BigDecimal, BigDecimal> computedAmounts = BigDecimal::add;
            summarySpendings.merge(category, transaction.getAmount(), (u, v) -> computedAmounts.apply(summarySpendings.get(category), transaction.getAmount()));
            totalAmount = totalAmount.add(transaction.getAmount());
        }
        summarySpendings.put("Total[€]", totalAmount);
        return summarySpendings;
    }

    public static BigDecimal calculateCO2EmissionsSpecificDates(LocalDate startDate, LocalDate endDate, List<Transaction> listOfTransactions) {
        BigDecimal CO2Emissions = new BigDecimal(0);
        for (Transaction transaction : listOfTransactions) {
            if (isBetweenDates(startDate, endDate, transaction)) {
                CO2Emissions = CO2Emissions.add(transaction.calculateCO2EmissionInGrams());
            }
        }
        return CO2Emissions;
    }

    public static BigDecimal calculateCO2EmissionsForTransList(List<Transaction> listOfTransactions) {
        BigDecimal CO2Emissions = new BigDecimal(0);
        for (Transaction transaction : listOfTransactions) {
            CO2Emissions = CO2Emissions.add(transaction.calculateCO2EmissionInGrams());
        }
        return CO2Emissions;
    }


    public static boolean isBetweenDates(LocalDate startDate, LocalDate endDate, Transaction transaction) {
        return (transaction.getDate().isAfter(startDate) && transaction.getDate().isBefore(endDate)) || transaction.getDate().equals(startDate) || transaction.getDate().equals(endDate);
    }

    public static Map<String, Map<String, BigDecimal>> calculateSpendingsInCO2(List<Transaction> listOfTransactions) {
        Map<String, Map<String, BigDecimal>> spendings = new HashMap<>();
        Map<String, BigDecimal> monthlySpendings;
        List<Transaction> listMonthly = new ArrayList<>();
        for (int j = 0; j < listOfTransactions.size() - 1; ++j) {
            Transaction actualTransaction = listOfTransactions.get(j);
            Transaction nextTransaction = listOfTransactions.get(j + 1);

            String month = String.valueOf(actualTransaction.getDate().getMonth());
            int year = actualTransaction.getDate().getYear();
            String nextMonth = String.valueOf(nextTransaction.getDate().getMonth());
            listMonthly.add(actualTransaction);
            if (!month.equals(nextMonth)) {
                monthlySpendings = calculateSummarySpendingsInCO2(listMonthly);
                spendings.put(year + "-" + month, monthlySpendings);

                listMonthly.removeAll(listMonthly);
            }
            if ((j == listOfTransactions.size() - 2)) {
                listMonthly.add(nextTransaction);
                monthlySpendings = calculateSummarySpendingsInCO2(listMonthly);
                spendings.put(year + "-" + month, monthlySpendings);
                listMonthly.removeAll(listMonthly);
            }

        }
        return spendings;
    }

    public static Map<String, Map<String, BigDecimal>> calculateSpendingsInEuro(List<Transaction> listOfTransactions) {
        Map<String, Map<String, BigDecimal>> spendings = new HashMap<>();
        Map<String, BigDecimal> monthlySpendings;
        List<Transaction> listMonthly = new ArrayList<>();
        for (int j = 0; j < listOfTransactions.size() - 1; ++j) {
            Transaction actualTransaction = listOfTransactions.get(j);
            Transaction nextTransaction = listOfTransactions.get(j + 1);
            ////test

            String month = String.valueOf(actualTransaction.getDate().getMonth());
            int year = actualTransaction.getDate().getYear();
            String nextMonth = String.valueOf(nextTransaction.getDate().getMonth());
            listMonthly.add(actualTransaction);
            if (!month.equals(nextMonth)) {
                monthlySpendings = calculateSummarySpendingsInEuro(listMonthly);
                spendings.put(year + "-" + month, monthlySpendings);

                listMonthly.removeAll(listMonthly);
            }
            if ((j == listOfTransactions.size() - 2)) {
                listMonthly.add(nextTransaction);
                monthlySpendings = calculateSummarySpendingsInEuro(listMonthly);
                spendings.put(year + "-" + month, monthlySpendings);
                listMonthly.removeAll(listMonthly);
            }

        }
        return spendings;
    }

    public static Map<String, Map<String, BigDecimal>> calculateSpendingsBetweenDatesInCO2(LocalDate startDate, LocalDate endDate, List<Transaction> listOfTransactions) {
        Map<String, Map<String, BigDecimal>> spendings = new HashMap<>();
        Map<String, BigDecimal> monthlySpendings;
        List<Transaction> listMonthly = new ArrayList<>();
        /*for (int j = 0; j < listOfTransactions.size() - 1; ++j){
            Transaction actualTransaction = listOfTransactions.get(j);
            Transaction nextTransaction = listOfTransactions.get(j + 1);

            if (isBetweenDates(startDate, endDate, actualTransaction)) {
                String month = String.valueOf(actualTransaction.getDate().getMonth());
                String nextMonth = String.valueOf(nextTransaction.getDate().getMonth());
                listMonthly.add(actualTransaction);
                if (!month.equals(nextMonth)) {
                    //monthlySpendings = calculateSummarySpendingsBetweenDates(startDate, endDate, listMonthly);
                    monthlySpendings = calculateSummarySpendingsInCO2(listMonthly);
                    spendings.put(month, monthlySpendings);

                    listMonthly.removeAll(listMonthly);
                }
                if ((j == listOfTransactions.size() - 2) && (isBetweenDates(startDate, endDate, nextTransaction))){
                    listMonthly.add(nextTransaction);
                    //monthlySpendings = calculateSummarySpendingsBetweenDates(startDate, endDate, listMonthly);
                    monthlySpendings = calculateSummarySpendingsInCO2(listMonthly);
                    spendings.put(month, monthlySpendings);
                    listMonthly.removeAll(listMonthly);
                }
            }
        }*/
        for (Transaction transaction :
                listOfTransactions) {
            if (isBetweenDates(startDate, endDate, transaction)) {
                listMonthly.add(transaction);
            }
        }
        monthlySpendings = calculateSummarySpendingsInCO2(listMonthly);
        spendings.put("Aggregate", monthlySpendings);
        return spendings;
    }

    public static Map<String, Map<String, BigDecimal>> calculateSpendingsBetweenDatesInEuro(LocalDate startDate, LocalDate endDate, List<Transaction> listOfTransactions) {
        Map<String, Map<String, BigDecimal>> spendings = new HashMap<>();
        Map<String, BigDecimal> monthlySpendings;
        List<Transaction> listMonthly = new ArrayList<>();
       /* for (int j = 0; j < listOfTransactions.size() - 1; ++j){
            Transaction actualTransaction = listOfTransactions.get(j);
            Transaction nextTransaction = listOfTransactions.get(j + 1);

            if (isBetweenDates(startDate, endDate, actualTransaction)) {
                String month = String.valueOf(actualTransaction.getDate().getMonth());
                String nextMonth = String.valueOf(nextTransaction.getDate().getMonth());
                listMonthly.add(actualTransaction);
                if (!month.equals(nextMonth)) {
                    //monthlySpendings = calculateSummarySpendingsBetweenDates(startDate, endDate, listMonthly);
                    monthlySpendings = calculateSummarySpendingsInEuro(listMonthly);
                    spendings.put(month, monthlySpendings);

                    listMonthly.removeAll(listMonthly);
                }
                if ((j == listOfTransactions.size() - 2) && (isBetweenDates(startDate, endDate, nextTransaction))){
                    listMonthly.add(nextTransaction);
                    //monthlySpendings = calculateSummarySpendingsBetweenDates(startDate, endDate, listMonthly);
                    monthlySpendings = calculateSummarySpendingsInEuro(listMonthly);
                    spendings.put(month, monthlySpendings);
                    listMonthly.removeAll(listMonthly);
                }
            }
        }*/
        for (Transaction transaction :
                listOfTransactions) {
            if (isBetweenDates(startDate, endDate, transaction)) {
                listMonthly.add(transaction);
            }
        }
        monthlySpendings = calculateSummarySpendingsInEuro(listMonthly);
        spendings.put("Aggregate", monthlySpendings);
        return spendings;
    }

    public void addTransactionTestUnused(Transaction transaction) {
        transaction.setCategory(transaction.getCategorisation());
        transaction.setDate(LocalDate.now());
        transactionsRepository.save(transaction);
    }

    public void addTransaction(String receiverNameAccount, String receiverIBAN, String amount, String description, String senderIBAN, String username) {
        if (userHasThisIBAN(username, senderIBAN)) {
            if (getBalance(senderIBAN, username).compareTo(new BigDecimal(amount)) >= 0) {
                String senderNameAccount = userRepository.getFullNameByUsername(username);
                Transaction transaction = new Transaction(senderIBAN, senderNameAccount, receiverIBAN, receiverNameAccount, new BigDecimal(amount), description, LocalDate.now());
                transaction.setCategory(transaction.getCategorisation());
                transaction.setCO2(transaction.calculateCO2EmissionInGrams());
                System.out.println(transaction);
                transactionsRepository.save(transaction);
            }
        }
        //transaction.setCategory(transaction.getCategorisation());
        //transaction.setDate(LocalDate.now());
        // transactionsRepository.save(transaction);
    }

    public void addTransactionTest(Transaction transaction, String username) {
        System.out.println(transaction);
        if (!userHasThisIBAN(username, transaction.getSenderIBAN())) {
            return;
        }
        if (getBalance(transaction.getSenderIBAN(), username).compareTo(transaction.getAmount()) >= 0) {
            String senderNameAccount = userRepository.getFullNameByUsername(username);
            transaction.setSenderNameAccount(senderNameAccount);
            transaction.setDate(LocalDate.now());
            //Transaction transaction = new Transaction(senderIBAN, senderNameAccount, receiverIBAN, receiverNameAccount, new BigDecimal(amount), description, LocalDate.now());
            transaction.setCategory(transaction.getCategorisation());
            transaction.setCO2(transaction.calculateCO2EmissionInGrams());
            System.out.println(transaction);
            transactionsRepository.save(transaction);
        }


        //transaction.setCategory(transaction.getCategorisation());
        //transaction.setDate(LocalDate.now());
        // transactionsRepository.save(transaction);
    }

    public void updateTransaction(String receiverIBAN, BigDecimal amount, String senderIBAN, String description, String newCategory, String username) {
        if ((userHasThisIBAN(username, senderIBAN)) || (userHasThisIBAN(username, receiverIBAN))) {
            Transaction transaction = transactionsRepository.getTransactionByAmountAndSenderIBANAndReceiverIBANAndDescription(amount, senderIBAN, receiverIBAN, description);
            System.out.println(transaction);
            transaction.setCategory(newCategory);
            transaction.setCO2(transaction.calculateCO2EmissionInGrams());
            transactionsRepository.save(transaction);
        }
    }


    public static List<Transaction> getSpecificTransactions(LocalDate startDate, LocalDate endDate, List<Transaction> listOfTransactions) {
        List<Transaction> transactionsList = new ArrayList<>();
        for (Transaction transaction : listOfTransactions
        ) {
            if (isBetweenDates(startDate, endDate, transaction)) {
                transactionsList.add(transaction);
            }
        }
        return transactionsList;
    }

    /*public List<Transaction> getTransBetweenDates(String IBAN, LocalDate startDate, LocalDate endDate){
        List<Transaction> transactionsList = getTransactionLists(IBAN);
        return getSpecificTransactions(startDate, endDate, transactionsList);
    }*/

    public BigDecimal calculateBalance(List<Transaction> listOfTransactions, String IBAN) {
        BigDecimal balance = new BigDecimal(0);
        for (Transaction transaction : listOfTransactions) {
            if (transaction.getReceiverIBAN().equals(IBAN)) {
                balance = balance.add(transaction.getAmount());
            } else if (transaction.getSenderIBAN().equals(IBAN)) {
                balance = balance.subtract(transaction.getAmount());
            }
        }
        return balance;
    }

    public BigDecimal getBalance(String IBAN, String username) {
        List<Transaction> transactionsList = getTransactionLists(IBAN, username);
        BigDecimal balance = calculateBalance(transactionsList, IBAN);
        return balance;
    }

}
