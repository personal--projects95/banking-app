package nl.rabobank.bankingapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;

@Configuration
public class SecurityConfig {

    @Autowired
    private CustomUserDetailsService userDetailsService;
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        /*CsrfTokenRequestHandler requestHandler = (HttpServletRequest request, HttpServletResponse response,
                Supplier< CsrfToken > deferredCsrfToken) -> {
            deferredCsrfToken.get();
            new XorCsrfTokenRequestAttributeHandler().handle(request, response, deferredCsrfToken);
        };*/
        return http
                    .userDetailsService(userDetailsService)
                    .headers((headers) -> headers.frameOptions((frameOptions) -> frameOptions.sameOrigin()))
                    //.csrf(csrf -> csrf
                     //   .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
                     //   .csrfTokenRequestHandler(requestHandler))
                    .csrf(csrf -> csrf.disable())
                    .cors(cors -> cors.configurationSource(corsConfigurationSource()))
                    .authorizeHttpRequests(auth -> auth
                            .requestMatchers(HttpMethod.OPTIONS).permitAll()
                            .requestMatchers("/h2-console/**").permitAll()
                            .anyRequest().authenticated())
                    .httpBasic(Customizer.withDefaults())
                    .build();
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource(){
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("http://localhost:4200", "https://www.google.com"));
        configuration.setAllowedHeaders(Arrays.asList("*"));
        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTION"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    /*@Bean
    UserDetailsService userDetailsService(){
        UserDetails bogdan = User
                                .withUsername("bogdan.bortosu")
                                .password(passwordEncoder().encode("bogdanBortosu"))
                                .build();
        UserDetails test = User
                                .withUsername("test.test")
                                .password(passwordEncoder().encode("testTest"))
                                .build();
        return  new InMemoryUserDetailsManager(bogdan, test);
    }*/

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}


