package nl.rabobank.bankingapp;

import nl.rabobank.bankingapp.accounts.Account;
import nl.rabobank.bankingapp.accounts.repositories.AccountRepository;
import nl.rabobank.bankingapp.transactions.Transaction;
import nl.rabobank.bankingapp.transactions.repositories.TransactionsRepository;
import nl.rabobank.bankingapp.users.User;
import nl.rabobank.bankingapp.users.repositories.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;

@Component
public class DatabasePopulation {

    UserRepository userRepository;
    AccountRepository accountRepository;
    TransactionsRepository transactionsRepository;

    PasswordEncoder passwordEncoder;

    public DatabasePopulation(UserRepository userRepository, AccountRepository accountRepository, TransactionsRepository transactionsRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.accountRepository = accountRepository;
        this.transactionsRepository = transactionsRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public void populateDB() throws IOException {
        populateUsers();
        populateAccounts();
        populateTransactions();
    }

    private void populateUsers() {
        User user1 = new User();
        user1.setUsername("bogdan.bortosu");
        user1.setFullName("Bogdan Bortosu");
        user1.setPassword(passwordEncoder.encode("bogdanBortosu"));
        User user2 = new User();
        user2.setUsername("test.test");
        user2.setFullName("test test");
        user2.setPassword(passwordEncoder.encode("testTest"));
        userRepository.save(user1);
        userRepository.save(user2);
    }

    private void populateTransactions() throws IOException {
        String line = "";
        String splitBy = ";";

        InputStream resourceAsStream = getClass().getResourceAsStream("/bank_transactions_all_v3.csv");
        BufferedReader br = new BufferedReader(new InputStreamReader(resourceAsStream));
        while ((line = br.readLine()) != null)
        {
            String[] row = line.split(splitBy);
            String senderNameAccount = row[0];
            String senderIBAN = row[1];
            //Account senderAccount = accountRepository.getAccountByIBAN(senderIBAN);
            BigDecimal amount = new BigDecimal(row[2]);
            String receiverNameAccount = row[3];
            String receiverIBAN = row[4];
            //Account receiverAccount = accountRepository.getAccountByIBAN(receiverIBAN);
            String description = row[5];
            LocalDate date = LocalDate.parse(row[6]);

            Transaction transaction = new Transaction(senderIBAN, senderNameAccount, receiverIBAN, receiverNameAccount, amount, description, date);
            transaction.setCategory(transaction.getCategorisation());
            transaction.setCO2(transaction.calculateCO2EmissionInGrams());
            transactionsRepository.save(transaction);
        }
    }

    private void populateAccounts() throws IOException {
        String line = "";
        String splitBy = ";";
        InputStream resourceAsStream = getClass().getResourceAsStream("/accounts.csv");
        BufferedReader br = new BufferedReader(new InputStreamReader(resourceAsStream));
        while ((line = br.readLine()) != null)
        {
            String[] row = line.split(splitBy);
            String username = row[0];
            User user = userRepository.getBankUserByUsername(username);
            String iban = row[1];
            Account customerAccount = new Account();
            customerAccount.setIBAN(iban);
            customerAccount.setOwner(user);
            accountRepository.save(customerAccount);
        }
    }
}
