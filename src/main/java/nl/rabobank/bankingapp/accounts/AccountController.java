package nl.rabobank.bankingapp.accounts;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class AccountController {

    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }


   @GetMapping( "/accounts")
    public List<String> customerAccounts(@RequestParam String customerName, Principal principal) {
        //TODO implement for mutliple users
        //String customerName = "Bogdan Bortosu";
       List<String> accounts = new ArrayList<>();
       if (principal.getName().equals(customerName)) {
           accounts = accountService.getAllAcounts(customerName);
           return accounts;
       }
       return accounts;

    }

}
