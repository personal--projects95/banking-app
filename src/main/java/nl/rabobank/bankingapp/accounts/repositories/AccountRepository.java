package nl.rabobank.bankingapp.accounts.repositories;

import nl.rabobank.bankingapp.accounts.Account;
import nl.rabobank.bankingapp.users.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
    @Query(value = "select IBAN from Account where owner.username = ?1")
    List<String> getIbansByUsername(String owner);




}
