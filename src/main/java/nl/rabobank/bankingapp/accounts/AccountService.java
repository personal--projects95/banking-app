package nl.rabobank.bankingapp.accounts;

import nl.rabobank.bankingapp.accounts.repositories.AccountRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountService {

    private AccountRepository accountRepository;

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public List<String> getAllAcounts(String customerName){
        List<String> accounts = accountRepository.getIbansByUsername(customerName);
        return accounts;
    }


}
