package nl.rabobank.bankingapp.accounts;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import nl.rabobank.bankingapp.transactions.Transaction;
import nl.rabobank.bankingapp.users.User;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Entity
@Table(name = "accounts")
public class Account {

    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    private User owner;

    @Column(nullable = false, length = 30)
    private String IBAN;

    //@OneToMany(mappedBy = "senderAccount")
   // private List<Transaction> outgoingTransactions;

   // @OneToMany(mappedBy = "receiverAccount")
   // private List<Transaction> incomingTransactions;


    public String getIBAN() {
        return IBAN;
    }

    public void setIBAN(String IBAN) {
        this.IBAN = IBAN;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Account() {

    }


}
