package nl.rabobank.bankingapp.users;

import lombok.RequiredArgsConstructor;
import nl.rabobank.bankingapp.transactions.Transaction;
import nl.rabobank.bankingapp.transactions.TransactionService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

    private final UserService userService;

//    public UserController(UserService userService) {
//        this.userService = userService;
//    }

    @GetMapping("/login")
    public boolean transactionsList(@RequestParam String username, @RequestParam String password) {
        return userService.isUser(username, password);
    }

}