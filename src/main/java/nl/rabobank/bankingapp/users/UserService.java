package nl.rabobank.bankingapp.users;

import nl.rabobank.bankingapp.users.repositories.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    UserRepository userRepository;

    public UserService(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    public boolean isUser(String username, String password){
        return userRepository.existsUserByUsernameAndPassword(username, password);
    }

}
