package nl.rabobank.bankingapp.users.repositories;

import nl.rabobank.bankingapp.users.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
    boolean findBankUserByUsernameAndPassword (String username, String password);
    boolean existsUserByUsernameAndPassword(String username, String password);

    @Query(value = "select fullName from User where username = ?1")
    String getFullNameByUsername(String username);

    User getBankUserByUsername(String username);
}
