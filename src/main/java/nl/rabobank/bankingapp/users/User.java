package nl.rabobank.bankingapp.users;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import nl.rabobank.bankingapp.accounts.Account;

import java.util.Set;

@Entity
@Table(name = "users")
public class User {

    @Id
    @Column(length = 50, unique = true)
    private String username;

    @Column(nullable = false, length = 200)
    private String fullName;

    @Column(nullable = false, length = 300)
    private String password;


    @OneToMany(mappedBy = "owner")
    private Set<Account> bankAccount;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Account> getAccount() {
        return bankAccount;
    }

    public void setAccount(Set<Account> bankAccount) {
        this.bankAccount = bankAccount;
    }
}

