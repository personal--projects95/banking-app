package nl.rabobank.bankingapp;

import lombok.RequiredArgsConstructor;
import nl.rabobank.bankingapp.accounts.repositories.AccountRepository;
import nl.rabobank.bankingapp.transactions.repositories.TransactionsRepository;
import nl.rabobank.bankingapp.users.User;
import nl.rabobank.bankingapp.users.repositories.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private UserRepository userRepository;

    public CustomUserDetailsService( UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = Optional.ofNullable(userRepository.getBankUserByUsername(username));

        if (user.isEmpty()){
            throw new UsernameNotFoundException("User not found!");
        }

        return org.springframework.security.core.userdetails.User
                .withUsername(user.get().getUsername())
                .password(user.get().getPassword())
                .build();
    }
}
