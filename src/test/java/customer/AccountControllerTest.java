package customer;

import nl.rabobank.bankingapp.Application;
import nl.rabobank.bankingapp.accounts.AccountService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;


import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.CoreMatchers.is;

@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = Application.class)
class AccountControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AccountService accountService;

    @Test
    void validRequestWithExistingCustomer() throws Exception {
        when(accountService.getAllAcounts("Bogdan Bortosu")).thenReturn(List.of("NL01BOG1122334455"));

        mvc
                .perform(get("/accounts").param("customerName", "Bogdan Bortosu"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0]", is("NL01BOG1122334455")));

    }

    @Test
    void validRequestWithNoExistingCustomer() throws Exception {
       // when(customerService.getAllAcounts("Bogdan Bortosu")).thenReturn(List.of("NL01BOG1122334455"));

        mvc
                .perform(get("/accounts").param("customerName", "Bogdan Bortosu"))
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));
    }
}